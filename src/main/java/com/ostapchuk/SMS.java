package com.ostapchuk;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMS {
    public static final String ACCOUNT_SID =
            "AC10b2177c95e1a018ca75d73b64f8fc59";
    public static final String AUTH_TOKEN =
            "e165986e77e84027e9f540476317fe50";

    public static void send(String str){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message.creator(new PhoneNumber("+380664975614"), // to
                        new PhoneNumber("+14049946654"), // from
                        str)
                .create();
    }
}
