package com.ostapchuk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main class, learning how to log
 *
 * @version 1.0.0
 * @author Ivan Ostapchuk
 */
public class Main {
    private static final Logger logger1 = LogManager.getLogger(Main.class);

    public static final String ACCOUNT_SID =
            "AC10b2177c95e1a018ca75d73b64f8fc59";
    public static final String AUTH_TOKEN =
            "e165986e77e84027e9f540476317fe50";

    public static void main(String[] args) {
        logger1.trace("This is a trace message");
        logger1.debug("This is a debug message");
        logger1.info("This is an info message");
        logger1.warn("This is a warn message");
        logger1.error("This is an error message");
        logger1.fatal("This is a fatal message\n");

    }
}
